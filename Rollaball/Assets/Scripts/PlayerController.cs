using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{

	// Create public variables for player speed, and for the Text UI game objects
	public float speed;
	public TextMeshProUGUI countText;
	public GameObject winTextObject;
	public CameraShake CameraShake;
	public float jumpforce = 0.50f;
	public float gravityVal = -9.80f;

	private float movementX;
	private float movementY;


	private Rigidbody rb;
	private int count;

	// At the start of the game...
	void Start()
	{
		

		// Assign the Rigidbody component to our private rb variable
		rb = GetComponent<Rigidbody>();

		// Set the count to zero 
		count = 0;

		SetCountText();

		// Set the text property of the Win Text UI to an empty string, making the 'You Win' (game over message) blank
		winTextObject.SetActive(false);
	}


	void FixedUpdate()
	{
		// Create a Vector3 variable, and assign X and Z to feature the horizontal and vertical float variables above
		Vector3 movement = new Vector3(movementX, 0.0f, movementY);

		rb.AddForce(movement * speed);


		if (Input.GetKey(KeyCode.Space))
		{
		Vector3 balljump = new Vector3 (0.0f, 6.0f, 0.0f);
		rb.AddForce(balljump * speed);
		}
		
		//StartCoroutine(CameraShake.Shake(.15f, .4f));

	}



	void OnTriggerEnter(Collider other)
	{

		// ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);

			

			// Add one to the score variable 'count'
			count = count + 1;

			// Run the 'SetCountText()' function (see below)
			SetCountText();
		}
	}

	void OnMove(InputValue value)
	{
		Vector2 v = value.Get<Vector2>();

		

		movementX = v.x;
		movementY = v.y;
	}

	void OnJump(InputValue value)
    {

		//if (Input.GetKey(KeyCode.Space)) ;
		//{
		//Vector3 balljump = new Vector3 (0.0f, 6.0f, 0.0f);
		//rb.AddForce(balljump * speed);
		//}

		//if (Input.GetKeyDown(KeyCode.Space))
		//{
		//	rb.velocity.y += Mathf.Sqrt("variable" * -3.0f * "gravity");
		//}

	}

	void SetCountText()
	{
		countText.text = "Count: " + count.ToString();

		if (count >= 39)
		{
			// change 13 to 39 later
			// Set the text value of your 'winText'
			winTextObject.SetActive(true);
		}
	}
}